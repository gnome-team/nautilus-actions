Document: nautilus-actions-es
Title: Nautilus-Actions Configuration Tool User's Manual
Author: Pierre Wieser, Frederic Ruaudel, Rodrigo Moya
Abstract: This manual describes what Nautilus-Actions
 Configuration Tool is and how it can be used.
Section: File Management

Format: PDF
Files: /usr/share/doc/nautilus-actions/pdf/es/nautilus-actions-config-tool.pdf.gz

Format: HTML
Index: /usr/share/doc/nautilus-actions/html/es/nact/nautilus-actions-config-tool.html
Files: /usr/share/doc/nautilus-actions/html/es/nact/*.html
